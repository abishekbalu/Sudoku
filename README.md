# Sudoku:
This is an application to generate a new Sudoku Puzzle anytime!! 

## Prerequisites:
1. An IDE (Integrated Development Environment)
2. gcc - Compiler / Any C/C++ Compiler.

## To Generate a new puzzle: 

1. First, clone the repository to your local machine. 

2. Navigate to the directory of the cloned repository. 

3. Open the file "Sudoku_Generator.cpp" file in an IDE. 

4. Build and Compile the file that you opened. 

5. Once the file is compiled, a puzzle and the solution to the puzzle is generated. 

6. Solve the puzzle and enjoy!! 

## Future Work: 

1. Save the generated puzzle and the solution to a file. 
